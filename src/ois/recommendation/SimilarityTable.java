package ois.recommendation;

public class SimilarityTable {
	public double[][] table;
	
	public SimilarityTable(int totalNrOfItems) {
		this.table = this.initiateTable(totalNrOfItems);
	}
	
	private double[][] initiateTable(int totalNrOfItems) {
		double[][] tmp = new double[totalNrOfItems][];
		for(int i = 0; i < totalNrOfItems; i++) {
			tmp[i] = new double[i + 1];
			tmp[i][i] = 1;
		}
		
		return tmp;
	}
	
	public double getSimilarityBetween(int item1, int item2) {
		if (item1 >= item2) {
			return table[item1][item2];
		} else {
			return table[item2][item1];
		}
		
	}
	
	public void setSimilarityBetween(int item1, int item2, double similarity) {
		if (item1 >= item2) {
			table[item1][item2] = similarity;
		} else {
			table[item2][item1] = similarity;
		}
	}
}
