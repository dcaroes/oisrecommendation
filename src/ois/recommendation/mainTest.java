package ois.recommendation;

import java.util.ArrayList;
import java.util.List;

public class mainTest {
	
	public static void printTable(SimilarityTable st) {
		for(double[] l : st.table) {
			for(double val : l){
				System.out.print(val);
				System.out.print(" | ");
			}
			System.out.println();
		}
	}
	
	public static void main(String[] args) {
		Item item1 = new Item(0, "boek1");
		Item item2 = new Item(1, "boek2");
		Item item3 = new Item(2, "boek3");
		
		Customer john = new Customer(0, "John");
		Customer mark = new Customer(1, "Mark");
		Customer lucy = new Customer(2, "Lucy");
		
		item1.addCustomerRequested(john);
		
		item2.addCustomerRequested(mark);
		item2.addCustomerRequested(lucy);
		
		item3.addCustomerRequested(john);
		item3.addCustomerRequested(mark);
		
		john.addRequestedItem(item1);
		john.addRequestedItem(item3);
		
		mark.addRequestedItem(item2);
		mark.addRequestedItem(item3);
		
		lucy.addRequestedItem(item2);
		
		List<Item> items = new ArrayList<Item>();
		List<Customer> customers = new ArrayList<Customer>();
		
		items.add(item1);
		items.add(item2);
		items.add(item3);
		
		customers.add(john);
		customers.add(mark);
		customers.add(lucy);
		
		Recommendation r = new Recommendation(items, customers);
		
		r.calculateItemSimilarityTable();
		
		printTable(r.getSimilarityTable());	
	}
}
