package ois.recommendation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Recommendation {
	private List<Item> productCatalog;
	private List<Customer> customers;
	private SimilarityTable similarityTable;
	
	public Recommendation(List<Item> items, List<Customer> customers) {
		this.productCatalog = new ArrayList<>();
		this.productCatalog = items;
		this.customers = customers;
	}
	
	public void calculateItemSimilarityTable() {
		int nrOfItems = productCatalog.size();
		int nrOfCustomers = customers.size();
		
		SimilarityTable st = new SimilarityTable(nrOfItems);
		double[][] rMatrix = new double[nrOfItems][nrOfCustomers];
		
		for(Item i1 : productCatalog) {
			for (Customer c : i1.getCustomersRequested()) {
				for(Item i2 : c.getRequestedItems()) {
					rMatrix[i1.getId()][c.getId()] = 1d;
					rMatrix[i2.getId()][c.getId()] = 1d;
				}
			}
			for (Item i2 : productCatalog) {
				double similarity = this.cosineSimilarity(rMatrix[i1.getId()], rMatrix[i2.getId()]);
				st.setSimilarityBetween(i1.getId(), i2.getId(), similarity);
			}
		}
		
		this.similarityTable = st;
	}
	
	public List<ItemWrapper> getRecommendationsFor(Item item) {
		int itemId = item.getId();
		List<ItemWrapper> nonZeroList = new ArrayList<ItemWrapper>();
		
		for(Item i : productCatalog) {
			double sim = this.similarityTable.getSimilarityBetween(itemId, i.getId());
			if(sim > 0.0 && sim < 0.99) {
				nonZeroList.add(new ItemWrapper(i, sim));
			}
		}
		
		Collections.sort(nonZeroList);
		return nonZeroList;
	}
	
	public SimilarityTable getSimilarityTable() {
		return this.similarityTable;
	}

	//http://stackoverflow.com/a/22913525
	private double cosineSimilarity(double[] vectorA, double[] vectorB) {
	    double dotProduct = 0.0;
	    double normA = 0.0;
	    double normB = 0.0;
	    for (int i = 0; i < vectorA.length; i++) {
	        dotProduct += vectorA[i] * vectorB[i];
	        normA += Math.pow(vectorA[i], 2);
	        normB += Math.pow(vectorB[i], 2);
	    }   
	    return dotProduct / (Math.sqrt(normA) * Math.sqrt(normB));
	}
	
	public class ItemWrapper implements Comparable<ItemWrapper>{
		public Item item;
		public double similarity;
		
		public ItemWrapper(Item i, double sim) {
			item = i;
			similarity = sim;
		}

		@Override
		public int compareTo(ItemWrapper o) {
			double compareVal = ((ItemWrapper) o).similarity;  
			if(compareVal <= similarity) {
				return -1;
			} else {
				return 1;
			}
		}
	}
}
