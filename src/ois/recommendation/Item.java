package ois.recommendation;

import java.util.ArrayList;
import java.util.List;

public class Item {
	private int id;
	private String tag;
	private List<Customer> requestedList;
	
	public Item(int id, String tag) {
		this.setId(id);
		this.setTag(tag);
		this.requestedList = new ArrayList<Customer>();
	}
	
	public void addCustomerRequested(Customer customer) {
		requestedList.add(customer);
	}
	
	public List<Customer> getCustomersRequested() {
		return requestedList;
	}

	public int getId() {
		return id;
	}

	private void setId(int id) {
		this.id = id;
	}
	
	public String getTag() {
		return tag;
	}

	private void setTag(String tag) {
		this.tag = tag;
	}
	
	

}
