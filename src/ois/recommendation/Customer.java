package ois.recommendation;

import java.util.ArrayList;
import java.util.List;

public class Customer {
	private int id;	
	private String tag;
	private List<Item> requestedItems;
	
	public Customer(int id, String tag) {
		this.setId(id);
		this.setTag(tag);
		this.requestedItems = new ArrayList<Item>();
	}
	
	public void addRequestedItem(Item item) {
		requestedItems.add(item);
	}

	public int getId() {
		return id;
	}
	
	private void setId(int id) {
		this.id = id; 
	}
	
	public List<Item> getRequestedItems() {
		return requestedItems;
	}

	public String getTag() {
		return tag;
	}

	private void setTag(String tag) {
		this.tag = tag;
	}

}
