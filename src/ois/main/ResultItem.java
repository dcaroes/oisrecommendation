package ois.main;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ResultItem {
	public Map<String, String> resultset = new HashMap<String, String>();
	
	@Override
	public String toString() {
		String result = "";
		Iterator it = resultset.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry pairs = (Map.Entry) it.next();
			result.concat(pairs.getKey() + ": " + pairs.getValue() + "/n");
			it.remove(); 
		}
		return result;
	}
	
}
