package ois.main;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import ois.recommendation.Customer;
import ois.recommendation.Item;
import ois.recommendation.Recommendation;
import ois.recommendation.Recommendation.ItemWrapper;
import ois.recommendation.SimilarityTable;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.shared.NotFoundException;

public abstract class RecommendationEngineFacade {
	private Recommendation recommendation;
	private ItemsAndCustomers itemAndCustomers;
	private DAO dao;
	
	
	public RecommendationEngineFacade(String pathToDump) {
		this.dao = new DAO(pathToDump);
		ResultSet rs = dao.userRequestItems();
		this.itemAndCustomers = getItemsAndCustomers(rs);
		this.recommendation = new Recommendation(
				new ArrayList<Item>(this.itemAndCustomers.items.values()),
				new ArrayList<Customer>(this.itemAndCustomers.customers.values()));
		this.recommendation.calculateItemSimilarityTable();
	}
	
	
	private ItemsAndCustomers getItemsAndCustomers(ResultSet rs) {
		ItemsAndCustomers result = new ItemsAndCustomers();
		
		Hashtable<String, Customer> customers = new Hashtable<String, Customer>();
		Hashtable<String, Item> items = new Hashtable<String, Item>();

		int currentCustomerId = 0;
		int currentItemId = 0;
		
		while (rs.hasNext()) {
		    QuerySolution row= rs.next();
		    RDFNode itemNode = row.get("product");
		    String itemString = itemNode.toString();
		    RDFNode customerNode = row.get("email");
		    String customerString =  customerNode.toString();
		    Item item = items.get(itemString);
		    
			System.out.println(itemString);

		    if(item != null) {
		    	Customer customer = customers.get(customerString);
		    	if (customer == null) {
		    		customer = new Customer(currentCustomerId, customerString);
		    		customer.addRequestedItem(item);
		    		currentCustomerId += 1;
		    		item.addCustomerRequested(customer);
		    		customers.put(customerString, customer);
		    	} else {
		    		customer.addRequestedItem(item);
		    		item.addCustomerRequested(customer);
		    	}
		    	
		    } else {
		    	Item tmpItem = new Item(currentItemId, itemString);
		    	
		    	Customer customer = customers.get(customerString);
		    	if (customer == null) {
		    		customer = new Customer(currentCustomerId, customerString);
		    		customer.addRequestedItem(tmpItem);
		    		currentCustomerId += 1;
		    		tmpItem.addCustomerRequested(customer);
		    		customers.put(customerString, customer);
		    	} else {
		    		customer.addRequestedItem(tmpItem);
		    		tmpItem.addCustomerRequested(customer);
		    	}
		    	
		    	items.put(itemString, tmpItem);
		    	currentItemId += 1;  
		    }
		   
		}
		
		result.customers = customers;
		result.items = items;

		return result;
	}
	
	protected List<ResultItem> getRecommendationsFor(String itemId) throws NotFoundException {
		Item i = this.itemAndCustomers.items.get(itemId);
		if (i == null) {
			throw new NotFoundException("No Item was found with id " + itemId);
		}
		List<ItemWrapper> tmp = recommendation.getRecommendationsFor(i);
		List<ResultItem> result = new ArrayList<ResultItem>();
		
		for(ItemWrapper iw : tmp) {
			ResultItem ri = new ResultItem();
			ri.resultset.put("Similarity", Double.toString(iw.similarity));
			
			ResultSet rs = this.dao.propertyValues("<" + iw.item.getTag() + ">");
			while(rs.hasNext()) {
				QuerySolution row = rs.next();
				RDFNode pnode = row.get("property");
				RDFNode vnode = row.get("value");
				String property = pnode.toString();
				String value = vnode.toString();
				ri.resultset.put(property, value);
			}
			result.add(ri);
		}
		
		return result;
	}
	
	public class ItemsAndCustomers {
		Hashtable<String, Customer> customers = new Hashtable<String, Customer>();
		Hashtable<String, Item> items = new Hashtable<String, Item>();
	}
	
}
