package ois.main;

import java.util.List;

import com.hp.hpl.jena.shared.NotFoundException;

public class WineRecommendationEngineFacade extends RecommendationEngineFacade {

	public WineRecommendationEngineFacade(String pathToDump) {
		super(pathToDump);
	}
	
	public List<ResultItem> getRecommendationFor(String name, String year) throws NotFoundException {
		String id = createId(name, year);
		return super.getRecommendationsFor(id);
	}
	
	private String createId(String name, String year) {
		String newName = name.replaceAll(" ", "_").replaceAll("é", "%C3");
		return "http://starpc14.vub.ac.be:8080/gospl/wine/" + year + "/" + newName;
	}
	

}
