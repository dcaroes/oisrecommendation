package ois.main;

import java.util.List;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.rdf.model.InfModel;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.reasoner.rulesys.GenericRuleReasoner;
import com.hp.hpl.jena.reasoner.rulesys.Rule;

public class DAO {
	
	private Model model;
	private final String prefix = 
			"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
			+ "PREFIX db: <http://genesis.rootcu.be:2020/resource/>"
			+ "PREFIX gosplAlcoholicBeverageCommunity: <http://starpc14.vub.ac.be:8080/gospl/ontology/20#>"
			+ "PREFIX owl: <http://www.w3.org/2002/07/owl#>"
			+ "PREFIX map: <http://genesis.rootcu.be:2020/resource/#>"
			+ "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>"
			+ "PREFIX gosplRequestCommunity: <http://starpc14.vub.ac.be:8080/gospl/ontology/16#>"
			+ "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
			+ "PREFIX gosplRequestItemCommunity: <http://starpc14.vub.ac.be:8080/gospl/ontology/19#>"
			+ "PREFIX gosplWineCommunity: <http://starpc14.vub.ac.be:8080/gospl/ontology/4#>"
			+ "PREFIX vocab: <http://genesis.rootcu.be:2020/resource/vocab/>"
    		+ "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>";
	
	public DAO(String RDFdoc){
		Model model = ModelFactory.createOntologyModel( OntModelSpec.OWL_MEM_MICRO_RULE_INF);
		model.read(RDFdoc);
		this.model=inferRules(model);
		
	}
	
	public DAO(Model m){
		this.model = inferRules(m);
	}
	
	private Model inferRules(Model model){
		List<Rule> rules = Rule.rulesFromURL("dump/rules.rules");
		GenericRuleReasoner r = new GenericRuleReasoner(rules);
		r.setOWLTranslation(true);           
		r.setTransitiveClosureCaching(true);
		InfModel infmodel = ModelFactory.createInfModel(r, model);
		return model.add(infmodel.getDeductionsModel());
	}
	
	private ResultSet execute(String sparqlQuery){
		Query query = QueryFactory.create(sparqlQuery);
		QueryExecution qe = QueryExecutionFactory.create(query, model);
		ResultSet results = qe.execSelect();
		//ResultSetFormatter.out(System.out, results, query);//debug
		return results;
	}
	
	public ResultSet userRequestItems(){
		String q = prefix
				//+ "SELECT * WHERE {?a ?b ?c}";
				+ "SELECT ?email ?product WHERE {"
				+ "?request gosplRequestCommunity:Request_consists_of_Request_item ?rfp_item."
				//+ "?request <http://starpc14.vub.ac.be:8080/gospl/ontology/9#emailaddress> ?email."
				+ "?rfp_item <http://starpc14.vub.ac.be:8080/gospl/ontology/9#emailaddress> ?email."//is now inferred
				+ "?rfp_item gosplRequestItemCommunity:Product ?product."
				+ " }";
		
		return execute(q);
		
	}
	
	public ResultSet propertyValues(String product){
		String q = prefix
				+ "SELECT * WHERE {"
				+ product + "?property ?value"
				+ " }";
		return execute(q);
	}
		
	
	
}
