package ois.main;

import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class Main {
		
	public static void main(String[] args) {
		if(args.length < 2){
			System.out.println("Provide wine name and year to the program.");
		}else{
	        String name =  args[0];
	        String year = args[1];
	        
			WineRecommendationEngineFacade re = new WineRecommendationEngineFacade("dump/output3.rdf");
			System.out.println();
			System.out.println("Recommendations for: " + name + " " + year + ":");
			System.out.println("----------------------------------------------------");
			System.out.println();
			try {
				List<ResultItem> results = re.getRecommendationFor(name, year);
				for (ResultItem i : results) {
					System.out.println(i.resultset.get("http://www.w3.org/2000/01/rdf-schema#label"));
					Iterator it = i.resultset.entrySet().iterator();
					while (it.hasNext()) {
						Map.Entry pairs = (Map.Entry) it.next();
						System.out.println("	" + pairs.getKey() + ": "
								+ pairs.getValue());
						it.remove(); // avoids a ConcurrentModificationException
					}
					System.out.println();
					System.out.println();
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());

			}
		}
	}
}
